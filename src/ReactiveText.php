<?php

namespace FifteenGroup\NovaCompactUi;

use FifteenGroup\NovaCompactUi\Traits\ReactiveField;
use Laravel\Nova\Fields\Text;

class ReactiveText extends Text
{
    use ReactiveField;

    public $component = 'reactive-text-field';
}
