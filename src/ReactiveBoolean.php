<?php

namespace FifteenGroup\NovaCompactUi;

use FifteenGroup\NovaCompactUi\Traits\ReactiveField;
use Laravel\Nova\Fields\Boolean;

class ReactiveBoolean extends Boolean
{
    use ReactiveField;

    public $component = 'reactive-boolean-field';
}
