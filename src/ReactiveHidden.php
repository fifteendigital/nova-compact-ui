<?php

namespace FifteenGroup\NovaCompactUi;

use FifteenGroup\NovaCompactUi\Traits\ReactiveField;
use Laravel\Nova\Fields\Hidden;

class ReactiveHidden extends Hidden
{
    use ReactiveField;

    public $component = 'reactive-hidden-field';
}
