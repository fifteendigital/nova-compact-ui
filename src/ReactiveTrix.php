<?php

namespace FifteenGroup\NovaCompactUi;

use FifteenGroup\NovaCompactUi\Traits\ReactiveField;
use Laravel\Nova\Fields\Trix;

class ReactiveTrix extends Trix
{
    use ReactiveField;

    public $component = 'reactive-trix-field';

    public function largeTextArea(): self
    {
        return $this->withMeta(['largeTextArea' => true]);
    }
}
